/**
 * Created by htmlmak on 06.02.2018.
 */

// Polyfill from https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith

if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function (searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        }
    });
}

function insertParam(key, value) {
    key = encodeURIComponent(key);
    value = encodeURIComponent(value);

    var s = document.location.search;
    var kvp = key + "=" + value;

    var r = new RegExp("(&|\\?)" + key + "=[^\&]*");

    s = s.replace(r, "$1" + kvp);

    if (!RegExp.$1) {
        s += (s.length > 0 ? '&' : '?') + kvp;
    }

    //again, do what you will here
    return s;
}

$(document).ready(function () {
    // Scroll to block function
    $(function () {
        window.scrollToBlock = function (target) {
            if (!target.jquery) {
                target = $(target);
            }
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 120
                }, 1000);
                return false;
            }
        };
    });

    // Change product count
    (function ($) {
        var changeCount = function (btn) {
            var $countBlock = btn.closest('.count');
            var currentValue = Number($countBlock.find('input').val());
            var newValue = null;

            // Up or down
            if (btn.find('.add').length) {
                newValue = currentValue + 1;
            } else {
                newValue = currentValue - 1;
            }

            if (newValue < 1) {
                newValue = 1
            }
            $countBlock.find('input')
                .val(newValue)
                .triggerHandler('changequantity', {
                    'new_value': newValue,
                    'old_value': currentValue
                })
            ;
        };

        $(document)
            .on('click.count', '.count__btn', function (e) {
                e.preventDefault();
                e.stopPropagation();

                changeCount($(this));
            })
            .on('change.count keyup.count', '.count input', function () {
                var newValue = Number(this.value.replace(/[^\d]/g, ''));

                if (newValue < 1) {
                    newValue = 1
                }

                this.value = newValue;
            })
            .on('cut copy paste', '.count input', function (event) {
                this.innerHTML = event.type + ' ' + this.value;
                return false;
            })
    })(jQuery);

    // Basket form UI
    (function ($) {
        var $changePersonalType = $('.change-personal-type');
        var changePersonalType = function () {
            if ($changePersonalType.find('input:checked').data('type') == 'individual') {
                $('#entity-fields').slideUp();
                $('#register-basket-block').find('input[name="company_name"]')
                    .prop('required', false)
                    .closest('.b-input').fadeOut(200);
            } else {
                $('#entity-fields').slideDown();
                $('#register-basket-block').find('input[name="company_name"]')
                    .prop('required', true)
                    .closest('.b-input').fadeIn(200);
            }
        };

        $changePersonalType.on('change', 'input', function () {
            changePersonalType();
        });
        changePersonalType();

        // File
        $('#order-requisites-file').on('change', function () {
            $('#requisites-file-name').text(this.files[0].name).fadeIn(200);
            $('#order-requisites-btn').find('span').text('Выбран файл:');

            /* TODO: временный костыль */
            if ($('#register-basket-block').find('input[name="company_name"]').val() !== '') {
                $('#requisites-basket').stop().slideUp();
            }
        });

        $('#order-requisites-btn').on('click', function () {
            $('#order-requisites-file').click();
        });

        // Requisites
        $('#write-requisites-open').on('click', function () {
            $('#requisites-basket').stop().slideToggle();
        });

        /* TODO: позже убрать */
        $('#register-basket-block button[type="submit"]').on('click', function (e) {
            if ($changePersonalType.find('input:checked').data('type') == 'entity' && $('#register-basket-block').find('input[name="company_name"]').val() == '') {
                $('#requisites-basket').stop().slideDown();
            }
        });


        $('#register-basket-block button[type="submit"]').on('click', function () {
            $('#register-basket-block form').addClass('touched');
        });
    })(jQuery);

    // Product detail
    (function ($) {
        $('.btn-more-wrapper').on('click', function (e) {
            e.preventDefault();
            $('#editions').find('.options-cards__list .column:nth-child(n+3)').slideDown();
            $(this).fadeOut(250);
        })
    })(jQuery);

    // Filters
    (function ($) {
        var checkboxes = $('#filter').find('.dropdown-pane input[type="checkbox"]');
        var getSerializeFilter = function () {
            var result = [];
            checkboxes.each(function () {
                if (this.checked) {
                    result.push(this.value);
                }
            });
            return result;
        };

        $(document)
            .on('change', '#filter .dropdown-pane input[type="checkbox"]', function (e) {
                $('#filter').trigger('changeFilter', {
                    filter: getSerializeFilter()
                });
            })
            .on('click', '#clear-filter', function () {
                window.location.href = '/'
            })
            .on('click', '.filter__selected button', function (e) {
                e.preventDefault();
                var _ = $(this);

                if (_.hasClass('close')) {
                    checkboxes.prop('checked', false);
                } else {
                    checkboxes
                        .filter(function () {
                            return this.value == _.data('value');
                        })
                        .prop('checked', false);
                }

                $('#filter').trigger('changeFilter', {
                    filter: getSerializeFilter()
                });
            })
            .on('click', 'a.edition-filter-btn', function (e) {
                if ($(this).attr('href') === '#' || window.innerWidth <= 1024) {
                    e.preventDefault();
                    return false;
                }
            })
            .on('change keydown keyup', '.filter-pane input[type="text"]', function (e) {
                // Динамический фильтр чекбоксов в "выпадающих" фильтрах
                var query = this.value;

                $(this).closest('.filter-pane').find('.b-checkbox').each(function () {
                    var _ = $(this);
                    var startWith = _.text().trim().toLowerCase().startsWith(query.toLowerCase(), 0);
                    if (startWith) {
                        _.fadeIn(0);
                    } else {
                        _.fadeOut(0);
                    }
                });
            })
            .on('changeFilter', '#filter', function (e, data) {
                // AJAX обновление страницы после фильтрации
                console.log('Filter:', e, data);
                var current_filters = data.filter;

                setTimeout(function () {
                    var query = insertParam('filters', current_filters.join(','));
                    setTimeout(function () {
                        history.pushState(null, null, query);

                        $.ajax({
                            url: query,
                            type: 'GET',
                            cache: true,
                            success: function (response) {
                                var $result = $(response);
                                if ($('.filter__selected').length) {
                                    $('.filter__selected').replaceWith($result.find('.filter__selected'));
                                } else {
                                    $('.filter__wrapper').append($result.find('.filter__selected'));
                                }
                                if ($('.pagination').length) {
                                    $('.pagination').replaceWith($result.find('.pagination'));
                                } else {
                                    $('.platform-list').after($result.find('.platform-list').next());
                                }
                                $('.platform-list').replaceWith($result.find('.platform-list'));
                            }
                        });
                    }, 50);
                }, 50)
            });

        $('.simple-bar').each(function () {
            new SimpleBar(this)
        });
    })(jQuery);

    // Assign order to manager
    (function ($) {
        var $resultModal = $('#assign-order-modal');
        $(document).on('click', '.assign-order-btn', function (e) {
            e.preventDefault();
            $.ajax({
                url: e.target.href,
                type: 'GET',
                dataType: 'json',
                cache: false,
                success: function (data) {
                    var message = '';
                    if (data.error_code == '0') {
                        message = data.message
                    } else {
                        message = data.error_message
                    }
                    $resultModal.find('h4').text(message);
                    $resultModal.foundation('open');
                },
                error: function () {
                    $resultModal.find('h4').text('При обработке произошла ошибка. Повторите попытку позже.');
                    $resultModal.foundation('open');
                }
            })

        });
    })(jQuery);

    // AJAX-form
    (function ($) {
        var $resultModal = $('#form-result-modal');
        $(document).on('submit', '.ajax-form', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var _ = $(this);
            $.ajax({
                url: e.target.action,
                type: 'POST',
                dataType: 'json',
                data: _.serialize(),
                cache: false,
                success: function (data) {
                    var message = '';
                    var visible_fields = _.find('input, select, textarea').filter(':visible');
                    if (data.error_code == '0') {
                        message = data.message
                    } else {
                        message = data.error_message
                    }

                    $resultModal.find('h4').text(message);
                    $resultModal.foundation('open');

                    setTimeout(function () {
                        visible_fields.val('');
                    }, 3000);
                }
            })

        });
    })(jQuery);


    // Input Mask
    (function ($) {
        $('#phone').inputmask("phone");
    })(jQuery);
});
