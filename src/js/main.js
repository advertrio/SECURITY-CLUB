(function ($) {
    $('#phone').inputmask("+7-999-999-9999");
})(jQuery);

(function ($) {
    var NeedTransferBlock = $('#need-transfer');
    var TransferFieldsBlock = $('#transfer-fields');
    var ChangeTransferStatus = function () {
        if (NeedTransferBlock.find('input:checked').data('type') == true) {
            TransferFieldsBlock
                .slideDown()
                .find('input').prop('required', true);
            NeedTransferBlock.find('.ordering-radio').addClass('disabled')
        } else {

            TransferFieldsBlock
                .slideUp()
                .find('input').prop('required', false)
            NeedTransferBlock.find('.ordering-radio').removeClass('disabled')
        }
    };

    NeedTransferBlock.on('change', 'input', function () {
        ChangeTransferStatus()
    });

    ChangeTransferStatus()

})(jQuery);


// Инициализируем плагин валидации
$.extend( $.validator.messages, {
    required: "Это поле необходимо заполнить.",
    remote: "Пожалуйста, введите правильное значение.",
    email: "Пожалуйста, введите корректный адрес электронной почты.",
    url: "Пожалуйста, введите корректный URL.",
    date: "Пожалуйста, введите корректную дату.",
    dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
    number: "Пожалуйста, введите число.",
    digits: "Пожалуйста, вводите только цифры.",
    creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
    equalTo: "Пожалуйста, введите такое же значение ещё раз.",
    extension: "Пожалуйста, выберите файл с правильным расширением.",
    maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
    minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
    rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
    range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
    max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
    min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
} );

var validationBehavior = {
    rules: {
        name: {
            required: true
        },
        surname: {
            required: true
        },
        email: {
            required: true,
            email: true
        },
        phone: {
            required: true,
            regx: /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/
        },

        company: {
            required: true
        }
    },
    messages: {
        name: "Поле \"Имя\" не заполнено",
        surname: "Поле \"Фамилия\" не заполнено",
        email: {
            required: "Укажите адрес электронной почты",
            email: "Неверный адрес электронной почты"
        },
        phone: {
            required: "Укажите номер телефона",
            regx: 'Неверный номер телефона'
        },

        company: {
            required: "Это обязательное поле",
        },

       position: {
            required: "Укажите вашу должность",
        }

    },
    highlight: function (element, errorClass) {
        $(element).addClass('form__field--state-error');
    },
    unhighlight: function (element, errorClass) {
        $(element).removeClass('form__field--state-error');
    },
    errorPlacement: function (error, element) {
        element.parent().append(error);
    },
    errorElement: "small",
    errorClass: "form__field-error-mess"
};

$('form').each(function (item, element) {
    $(element).validate(validationBehavior);
});